<?php
/*
 * @file FeedsUserContactProcessor.inc
 * Provides FeedsUserContactProcessor
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

/**
 * Feeds processor plugin. Create user contacts from feed items.
 */
class FeedsUserContactProcessor extends FeedsProcessor {

  /**
   * Implementation of FeedsProcessor::process().
   */
  public function process(FeedsImportBatch $batch, FeedsSource $source) {

    global $user;
    // Count number of created and updated nodes.
    $created  = $updated = $failed = $invalid_country = $invalid_state = 0;

    $country_codes = array_keys(location_get_iso3166_list(TRUE));
    $countries = array_change_key_case(array_flip(location_get_iso3166_list(FALSE)), CASE_UPPER);

    while ($item = $batch->shiftItem()) {

      if (!($id = $this->existingItemId($batch, $source)) || $this->config['update_existing']) {

        // Map item to a term.
        $user_contact = $this->map($batch);
        $user_contact->uid = $user->uid;

        $country_found = FALSE;
        if (isset($user_contact->country_cd)) {
          if (drupal_strlen($user_contact->country_cd) > 2) {
          //we have a country name or something else here
          //see if it is in our supported countries
          //we compare in ucase to remove case ambiguities
            if (isset($countries[drupal_strtoupper($user_contact->country_cd)])) {
              //we have this country - save the code instead (lowercase)
              $user_contact->country_cd = drupal_strtolower($countries[drupal_strtoupper($user_contact->country_cd)]);
              $country_found = TRUE;
            }
          }
          else {
            if (in_array(drupal_strtoupper($user_contact->country_cd), $country_codes, TRUE)) {
              //this is a valid country code
              $country_found = TRUE;
              //make sure it is in lower case
              $user_contact->country_cd = drupal_strtolower($user_contact->country_cd);
            }
          }
        }
        
        if (!$country_found) {
          if (isset($user_contact->country_cd)) {
            //there was a value here but it is not valid
            $invalid_country++;
          }
          unset($user_contact->country_cd);
        }
        else {
          //let's try and check/validate the state
          $state_found = FALSE;
          if (isset($user_contact->state_cd) &&
              ($state_cd = location_province_code($user_contact->country_cd, $user_contact->state_cd))) {
            //we have a valid state here - the function checks both state names and state codes
            $state_found = TRUE;
            $user_contact->state_cd = $state_cd;
          }
          
          if (!$state_found) {
            if (isset($user_contact->state_cd)) {
              //there was a value here but it is not valid
              $invalid_state++;
            }
            unset($user_contact->state_cd);
          }
        }       
        

        // Check if mail is set, otherwise continue.
        if (empty($user_contact->email) || !valid_email_address($user_contact->email)) {
          $failed++;
          continue;
        }

        // Add existing contact id if available.
        if (!empty($id)) {
          $user_contact->id = $id;
        }

        //set the import_status_flag
        if ($user_contact->import_status_flag != 0) { // we are not auto setting the review flag
          $fields = array('firstname', 'surname', 'address', 'city', 'country_cd', 'email', 'state_cd', 'postcode');
          foreach ($fields as $field)   {
            if (!isset($user_contact->{$field}) || empty($user_contact->{$field})) {
              //we are missing stuff - mark as requiring review
              $user_contact->import_status_flag = 0;
            }
          }
        }
        
        //let other modules have a go
        drupal_alter('user_contacts_feed_import', $user_contact);

        // Save the contact.
        user_contacts_save_contact($user_contact);

        if ($uid) {
          $updated++;
        }
        else {
          $created++;
        }
      }
    }

    // Set messages.
    if ($failed) {
      drupal_set_message(
        format_plural(
          $failed,
          'There was 1 contact that could not be imported because either their first name or their email was empty or not valid. You will need to <a href="!url">add this contact manually</a>.',
          'There were @count contacts that could not be imported because either their first name or their email was empty or not valid. You will need to <a href="!url">add these contacts manually</a>.',
          array('!url' => url('user/'. $user->uid .'/contacts/add'))
        ),
        'error'
      );
    }
    if ($invalid_country) {
      drupal_set_message(
        format_plural(
          $invalid_country,
          'There was 1 contact where the country provided could not be validated. You will need to <a href="!url">edit this contact manually</a>.',
          'There were @count contacts where the country provided could not be validated. You will need to <a href="!url">edit these contacts manually</a>.',
          array('!url' => url('user/'. $user->uid .'/contacts/imported'))
        ),
        'warning'
      );
    }
    if ($invalid_state) {
      drupal_set_message(
        format_plural(
          $invalid_state,
          'There was 1 contact where the state/province and/or country provided could not be validated. You will need to <a href="!url">edit this contact manually</a>.',
          'There were @count contacts where the state/province and/or country provided could not be validated. You will need to <a href="!url">edit these contacts manually</a>.',
          array('!url' => url('user/'. $user->uid .'/contacts/imported'))
        ),
        'warning'
      );
    }
    if ($created) {
      drupal_set_message(format_plural($created, 'Imported 1 contact.', 'Imported @count contacts.'));
    }
    elseif ($updated) {
      drupal_set_message(format_plural($updated, 'Updated 1 contact.', 'Updated @count contacts.'));
    }
    else {
      drupal_set_message(t('No contacts were imported or updated.'));
    }
  }

  /**
   * Implementation of FeedsProcessor::clear().
   */
  public function clear(FeedsBatch $batch, FeedsSource $source) {
    // Do not support deleting contacts
    throw new Exception(t('User contact processor does not support deleting contacts.'));
  }

  /**
   * Execute mapping on an item.
   */
  protected function map(FeedsImportBatch $batch) {
    // Prepare term object.
    $target_user_contact = new stdClass();
    $target_user_contact->id = FALSE;
    $target_user_contact->import_status_flag = $this->config['import_status_flag'];
    $target_user_contact->imported_flag = 1;

    // Have parent class do the iterating.
    return parent::map($batch, $target_user_contact);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'import_status_flag' => 1,
      'update_existing' => 1,
      'mappings' => array(),
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();

    $form['import_status_flag'] = array(
      '#type' => 'radios',
      '#title' => t('Mark for review'),
      '#description' => t('Select whether all imported contacts should be flagged as requiring review. Note records missing fields or where the state/country fail to validate will automatically be marked as requiring review.'),
      '#options' => array(0 => t('Mark all as review required'), 1 => t("Don't mark all as review required")),
      '#default_value' => $this->config['import_status_flag'],
    );

    $form['update_existing'] = array(
      '#type' => 'checkbox',
      '#title' => t('Replace existing contacts'),
      '#description' => t('If an existing contact is found in imported data, replace it. Existing contacts will be identified based on email address.'),
      '#default_value' => $this->config['update_existing'],
    );
    return $form;
  }

  /**
   * Set target element.
   */
  public function setTargetElement(&$target_item, $target_element, $value) {
    $target_item->$target_element = $value;
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = array(
      'firstname' => array(
        'name' => t('First name'),
        'description' => t('First name of the contact.'),
        'optional_unique' => FALSE,
      ),
      'surname' => array(
        'name' => t('Surname'),
        'description' => t('Surname of the contact.'),
        'optional_unique' => FALSE,
      ),
      'email' => array(
        'name' => t('Email address of the contact'),
        'description' => t('Email address of the user.'),
        'optional_unique' => TRUE,
      ),
      'address' => array(
        'name' => t('Address'),
        'description' => t('Address of the contact.'),
        'optional_unique' => FALSE,
      ),
      'city' => array(
        'name' => t('City'),
        'description' => t('City/Town of the contact.'),
        'optional_unique' => FALSE,
      ),
      'postcode' => array(
        'name' => t('Postal code'),
        'description' => t('Postal code of the contact.'),
        'optional_unique' => FALSE,
      ),
      'country_cd' => array(
        'name' => t('Country Code (iso 2-digit)'),
        'description' => t('Country of the contact (an attempt will be made to code names).'),
        'optional_unique' => FALSE,
      ),
      'state_cd' => array(
        'name' => t('State code'),
        'description' => t('State of the contact (an attempt will be made to code names).'),
        'optional_unique' => FALSE,
      ),
    );
    

    // Let other modules expose mapping targets.
    self::loadMappers();
    drupal_alter('feeds_user_contact_processor_targets', $targets);

    return $targets;
  }

  /**
   * Get id of an existing feed item term if available.
   */
  protected function existingItemId(FeedsImportBatch $batch, FeedsSource $source) {

    global $user;
    // Iterate through all unique targets and try to find a user for the
    // target's value.
    foreach ($this->uniqueTargets($batch) as $target => $value) {
      switch ($target) {
        case 'email':
          $id = db_result(db_query("SELECT id FROM {user_contacts}
                                   WHERE email = '%s'
                                   AND uid = %d", $value, $user->uid));
          break;
      }
      if ($id) {
        // Return the id found.
        return $id;
      }
    }
    return 0;
  }
}

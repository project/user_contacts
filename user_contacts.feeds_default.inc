<?php
/*
 * @file user_contacts.feeds_default.inc
 * Provides default feeds importer for loading contacts from csv
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

function _user_contacts_feeds_importer_default() {
  $feeds = array();
  
  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'csv_contacts';
  $feeds_importer->config = array(
    'name' => 'CSV Contacts',
    'description' => 'Import contacts from CSV',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsUserContactProcessor',
      'config' => array(
        'import_status_flag' => '1',
        'update_existing' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Firstname',
            'target' => 'firstname',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Surname',
            'target' => 'surname',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Email',
            'target' => 'email',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'Address',
            'target' => 'address',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'City',
            'target' => 'city',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Postcode',
            'target' => 'postcode',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Country',
            'target' => 'country_cd',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'State',
            'target' => 'state_cd',
            'unique' => FALSE,
          ),
        ),
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
  );
  
  $feeds['csv_contacts'] = $feeds_importer;
  
  return $feeds;
}
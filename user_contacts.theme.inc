<?php
// $Id$
/*
 * @file user_contacts.theme.inc
 * Provides theme functions for user_contacts module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html 
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

/**
 * Default implementation of theme_user_contacts_contacts_form
*/
function theme_user_contacts_contacts_form($form) {
  $account = $form['#account'];
  $letter = $form['#letter'];
  $import = $form['#import'];
  $suffix = '';
  if ($import) {
    $suffix = '/import';
  }
  $header = array(
    t('Name'),
    t('Address'),
    t('Email'),
    '&nbsp;'
  );
  
  $rows = $row = array();
  
  foreach (element_children($form['contacts']) as $id) {
    $user_contact = $form['contacts'][$id]['#value'];
    $row = array(
      array('data' => theme('user_contacts_name', $user_contact), 'class' => 'usc-name'),
      array('data' => theme('user_contacts_address_summary', $user_contact), 'class' => 'usc-address'),
      array('data' => check_plain($user_contact->email), 'class' => 'usc-email'),
      array('data' => l(t('Edit'), 'user/'. $account->uid .'/contacts/'. $id . $suffix,
                        array('attributes' => array('class' => 'edit-contact'))) .'&nbsp;'.
                      l(t('Delete'), 'user/'. $account->uid .'/contacts/'. $id .'/delete'. $suffix,
                        array('attributes' => array('class' => 'delete-contact'))), 'class' => 'usc-ops')
    );
    $rows[] = $row;
  }
  
  
  if (count($rows) == 0) {
    if ($letter) {
      $rows[] = array(array('data' => t('There are no matching contacts for the letter @letter, try <a class="colorbox add-contact" href="!url">viewing all contacts</a>',
                                        array('@letter' => $letter,
                                              '!url' => url('user/'. $account->uid .'/contacts'))), 'colspan' => 4));
    }
    else {
      $rows[] = array(array('data' => t('There are no contacts, please <a href="!url">add one</a>',
                                        array('@letter' => $letter,
                                              '!url' => url('user/'. $account->uid .'/contacts/add'))), 'colspan' => 4));
    }
  }
  
  $alpha_links = array();
  $chr_cd = 65;
  
  while ($chr_cd <= 90) {
    $attributes = array();
    if (chr($chr_cd) == drupal_strtoupper($letter)) {
      $attributes = array('class' => 'usc-active-letter');
    }
    $alpha_links[] = l(chr($chr_cd), 'user/'. $account->uid .'/contacts/address-book/'. chr($chr_cd), array('attributes' => $attributes));
    $chr_cd++;
  }
  $show_all_link = l(t('Show All'), 'user/'. $account->uid .'/contacts');
  $add_link = l(t('Add New Address'), 'user/'. $account->uid .'/contacts/add',
                array('attributes' => array('class' => 'add-contact colorbox')));

  return '
    <div class="contacts-link-wrapper"><div class="show-all-link">'. $show_all_link .'</div>
    <div class="add-new-link">'. $add_link .'</div></div>
    <div class="alpha-links">'. theme('item_list', $alpha_links) .'</div>'.
    theme('table', $header, $rows, array('class' => 'user_contacts')) .
    drupal_render($form);
}

/**
 * Default implementation of theme_user_contacts_address_summary
 * @param $user_contact object a user contact @see user_contacts_contact_load
*/
function theme_user_contacts_address_summary($user_contact) {
  //reuse some location goodness here
  $user_contact->postal_code = $user_contact->postcode;
  $user_contact->street = $user_contact->address;
  return location_address2singleline((array)$user_contact);
}

/**
 * Default implementation of theme_user_contacts_name
 * @param $user_contact object a user contact @see user_contacts_contact_load
 * @param $check_plain bool true to pass via check plain
*/
function theme_user_contacts_name($user_contact, $check_plain = TRUE) {
  if (!$check_plain) {
    return $user_contact->firstname .' '. $user_contact->surname;
  }
  return check_plain($user_contact->firstname .' '. $user_contact->surname);
}


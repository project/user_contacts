<?php
/*
 * @file user_contacts.util.inc
 * Util functions for user_contacts module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

/**
 * Fetch contacts the use imported that 
*/
function user_contacts_get_imported($state = 0, $letter = FALSE) {
  global $user;
  $query = "SELECT * FROM {user_contacts}
                  WHERE uid = %d
                  AND import_status_flag = %d
                  AND imported_flag = 1";
  $params = array($user->uid, $state);
  if ($letter) {
    $query .= "
     AND surname LIKE '%s%%'";
    $params[] = $letter;
  }
  $res = pager_query($query, 50, 0, NULL, $params);
  $contacts = array();
  while ($contact = db_fetch_object($res)) {
    $contacts[$contact->id] = $contact;
  }
  drupal_alter('user_contacts_contacts', $contacts);
  return $contacts;
}

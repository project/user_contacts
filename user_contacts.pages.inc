<?php
/*
 * @file user_contacts.pages.inc
 * Provides pages for the user_contacts module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

/**
 * Form builder for user contacts list
 * We build this as a form instead of a flat table
 * so other modules can modify it
 * we wouldn't need this in Drupal 7!
*/
function user_contacts_contacts_form($form_state, $account, $letter = FALSE) {
  if (drupal_strlen($letter) > 1) {
    //we only want one letter
    $letter = substr($letter, 0, 1);
  }
  $contacts = user_contacts_fetch_contacts($account, $letter, 50);

  $form = array();
  $form['#account'] = $account;
  $form['#letter'] = $letter;
  $form['#import'] = FALSE;
  foreach ($contacts as $contact_id => $contact) {
    $form['contacts'][$contact_id] = array(
      '#value' => $contact,
      '#type' => 'value'
    );
  }
  $form['pager'] = array(
    '#value' => theme('pager'),
    '#type' => 'markup'
  );
  return $form;
}

/**
 * Form builder for adding/editing a user contact:
 * @param $form_state array std form state array
 * @todo
 * @param $account object the user account associated with the contact
 * @param $user_contact object the user contact being edited/ FALSE if new
 * @param $return_import bool pass true to return to import errors on submit
 * @return array FAPI structured form array
*/
function user_contacts_contact_form($form_state, $account, $user_contact = FALSE,
                                    $return_import = FALSE) {
  $form = array();
  //Note there are 12 fields in the table
  //uid
  $form['uid'] = array('#type' => 'value', '#value' => $account->uid);
  $form['return_import'] = array('#type' => 'value', '#value' => $return_import);
  //id
  $form['id'] = array('#type' => 'value', '#value' => ($user_contact ? $user_contact->id : NULL));
  //imported flag
  $form['imported_flag'] = array('#type' => 'value', '#value' => ($user_contact ? $user_contact->imported_flag : 0));
  
  //contact name header
  $form['name_header'] = array(
    '#value' => '<h3 class="ucs-header">'. t('Contact Name') .'</h3>'
  );
  //firstname
  $form['firstname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('First Name'),
    '#default_value' => $user_contact ? $user_contact->firstname : NULL,
    '#size'          => 60,
    '#maxlength'     => 255,
    '#required'      => TRUE,
  );
  //surname (last name)
  $form['surname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Last Name'),
    '#default_value' => $user_contact ? $user_contact->surname : NULL,
    '#size'          => 60,
    '#maxlength'     => 255,
    '#required'      => TRUE,
  );
  //email (email)
  $form['email'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Email'),
    '#default_value' => $user_contact ? $user_contact->email : NULL,
    '#size'          => 60,
    '#maxlength'     => 255,
    '#required'      => TRUE,
  );
  //contact address header
   $form['address_header'] = array(
    '#value' => '<h3 class="ucs-header">'. t('Contact Address') .'</h3>'
  );
  //address
  $form['address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Address'),
    '#default_value' => $user_contact ? $user_contact->address: NULL,
    '#size'          => 60,
    '#maxlength'     => 255,
    '#required'      => TRUE,
  );
  //town
  $form['city'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Town'),
    '#default_value' => $user_contact ? $user_contact->city: NULL,
    '#size'          => 60,
    '#maxlength'     => 255,
    '#required'      => TRUE,
  );
  //county (state)
  $form['state'] = array(
    '#type'          => 'textfield',
    '#title'         => t('State/County'),
    '#default_value' => $user_contact ? $user_contact->state: NULL,
    '#size'          => 60,
    '#maxlength'     => 255,
    '#required'      => TRUE,
  );
  //postcode
  $form['postcode'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Postcode'),
    '#default_value' => $user_contact ? $user_contact->postcode: NULL,
    '#size'          => 60,
    '#maxlength'     => 16,
    '#required'      => TRUE,
  );
  //country
  $form['country_cd'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#default_value' => $user_contact ? $user_contact->country_cd: 'us',
    '#options' => location_get_iso3166_list(),
  );
  
  if ($user_contact && $user_contact->imported_flag && $user_contact->import_status_flag == 0) {
    //make sure we have a flag for 'remind me again' if import_status_flag is set
    $form['import_status_flag'] = array(
      '#type' => 'checkbox',
      '#title' => t('Mark this item as fixed'),
      '#default_value' => $user_contact->import_status_flag,
      '#description' => t('If you check this box and save, this contact will be removed from the import errors list.'),
    );
  }
  else {
    $form['import_status_flag'] = array('#type' => 'value', '#value' => 1);
  }
  
  $form['submit'] = array('#type' => 'submit', '#value' => ($user_contact ? t('Save') : t('Add')));
  return $form;
}

/**
 * Form validation handler for adding/editing a user contact:
 * @todo
 * @param $form array FAPI structured form array
 * @param $form_state array std form state array
*/
function user_contacts_contact_form_validate($form, &$form_state) {
  $vals = $form_state['values'];
  if (!valid_email_address($vals['email'])) {
    form_set_error('email', t('You must enter a valid email address'));
  }
  $province_cd = location_province_code($vals['country_cd'], $vals['state']);
  if (empty($province_cd)) {
    form_set_error('state', t('Please enter a valid state/county for the chosen country'));
  }
}

/**
 * Form submission handler for adding/editing a user contact:
 * @param $form array FAPI structured form array
 * @todo
 * @param $form_state array std form state array
*/
function user_contacts_contact_form_submit($form, &$form_state) {
  $user_contact = $form_state['values'];
  $user_contact['state_cd'] = location_province_code($user_contact['country_cd'], $user_contact['state']);
  user_contacts_save_contact($user_contact);
  drupal_set_message(t('Saved contact @name', array(
        '@name' => $user_contact['firstname'] .' '. $user_contact['surname']
      )));
  module_load_include('inc', 'user_contacts', 'user_contacts.util');
  if ($form_state['values']['return_import'] && count(user_contacts_get_imported()) > 0) {
    $form_state['redirect'] = 'user/'. $user_contact['uid'] .'/contacts/imported';    
  }
  else {
    $form_state['redirect'] = 'user/'. $user_contact['uid'] .'/contacts';
  }
}

/**
 * Form builder for deleting a contact:
 * @param $form_state array std form state array
 * @param $user_contact object a user contact object
 * @param $return_import bool pass true to return to import errors on submit
 * @return array FAPI structured form array
*/
function user_contacts_contact_delete_form($form_state, $user_contact = FALSE,
                                           $return_import = FALSE) {
  if (!$user_contact) {
    return drupal_not_found();
  }

  $form['return_import'] = array('#type' => 'value', '#value' => $return_import);

  $form['user_contact'] = array(
    '#type' => 'value',
    '#value' => $user_contact,
  );
  return confirm_form($form,
    t('Deleted contact @name?',
      array(
        '@name' => $user_contact->firstname .' '. $user_contact->surname
      )
    ),
    'user/'. $user_contact->uid .'/contacts',
    t('Are you sure, this cannot be undone'),
    t('Delete'),
    t('Cancel')
  );

  return $form;
}

/**
 * Form submission handler for deleting a contact:
 * @param $form array FAPI structured form array
 * @param $form_state array std form state array
*/
function user_contacts_contact_delete_form_submit($form, &$form_state) {
  if (!isset($form_state['values']['user_contact'])) {
    return drupal_not_found();
  }
  $user_contact = $form_state['values']['user_contact'];
  user_contacts_delete_contact($user_contact);
  drupal_set_message(
    t('Deleted contact @name',
      array(
        '@name' => $user_contact->firstname .' '. $user_contact->surname
      )
    )
  );
  module_load_include('inc', 'user_contacts', 'user_contacts.util');
  if ($form_state['values']['return_import'] && count(user_contacts_get_imported()) > 0) {
    $form_state['redirect'] = 'user/'. $user_contact->uid .'/contacts/imported';    
  }
  else {
    $form_state['redirect'] = 'user/'. $user_contact->uid .'/contacts';
  }
}
/**
 * Form builder for list of newly imported contacts requiring edit:
 * @param $form_state array std form state array
 * @return array FAPI structured form array
*/
function user_contacts_imported_form($form_state, $letter = FALSE) {
  drupal_set_message(t('The following contacts in your address book were imported but are still incomplete.'));
  if (drupal_strlen($letter) > 1) {
    //we only want one letter
    $letter = substr($letter, 0, 1);
  }

  module_load_include('inc', 'user_contacts', 'user_contacts.util');
  $contacts = user_contacts_get_imported(0, $letter);

  global $user;

  $form = array();
  $form['#account'] = $user;
  $form['#letter'] = $letter;
  $form['#import'] = TRUE;
  foreach ($contacts as $contact_id => $contact) {
    $form['contacts'][$contact_id] = array(
      '#value' => $contact,
      '#type' => 'value'
    );
  }
  $form['pager'] = theme('pager');
  $form['#theme'] = 'user_contacts_contacts_form';
  return $form;
}

/**
 * Menu callback to load the feeds form
 * We have to do this here so we can be sure the include is loaded
*/
function user_contacts_csv_form() {
  module_load_include('inc', 'feeds', 'feeds.pages');
  return drupal_get_form('feeds_import_form', 'csv_contacts');
}